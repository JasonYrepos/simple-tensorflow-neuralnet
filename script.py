import numpy as np
import tensorflow as tf

#three features, XOR the first two digits(features), third digit(feature) is not used 

x = np.array([[0,0,1],
              [0,1,1],
              [1,0,1],
              [0,1,0],
              [1,1,1],
              [0,0,0]
                      ]).astype(np.float32);
y = np.array([[0,1,1,1,0,0]]).T.astype(np.float32);

xt = tf.placeholder(tf.float32, [None,3])

w = tf.Variable( tf.random_normal([3,4]))
b = tf.Variable( tf.zeros([4]))

w2 = tf.Variable( tf.random_normal([4,1])) 
b2 = tf.Variable( tf.zeros([1]))

h1 = tf.nn.sigmoid(tf.matmul(xt,w,) +b) #hidden layer

h2 = tf.nn.sigmoid(tf.matmul(h1,w2) +b2) #final output
y_ = tf.placeholder(tf.float32, [None, 1])


cost = tf.reduce_mean(( (y_ * tf.log(h2)) + ((1 - y_) * tf.log(1.0 - h2)) ) * -1)
train_step = tf.train.GradientDescentOptimizer(0.01).minimize(cost)

sess = tf.InteractiveSession()
tf.global_variables_initializer().run()

for i in range(400000):
    sess.run(train_step, feed_dict={xt: x, y_: y })

nW = sess.run(w);
nW2 = sess.run(w2);
nb = sess.run(b);
nb2 = sess.run(b2);

test = np.array([[1,1,1]]).astype(np.float32)
#predict by feedforward test input into newly trained weights
output = sess.run(tf.nn.sigmoid(tf.matmul(test,nW) + nb))
output2 = tf.nn.sigmoid(tf.matmul(output,nW2) +nb2)

print(sess.run(output2))
#expected output is 0
